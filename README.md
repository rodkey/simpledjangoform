**simpledjangoform - a form that asks for a number and displays the last 10 numbers provided**

This is an example application created in Django for John Rodkey's CS-125 Database Design course.

The prompt is to create a web application that asks for a number, adds that number to a database, and displays the last 10 numbers that have been provided.

The overall site is called formsite, and the application is called ten .

Usage:  in a terminal, 
    1. change directory to the one that contains manage.py
    2. having already installed pipenv, do 'pipenv shell' to get into the proper python environment.
    3. Make sure the migrations are up to date by doing ' ./manage.py makemigrations ; ./manage.py migrate '
    4. Run the web server by doing ' pipenv run dev '
    5. Browse to http://localhost:8000/ten/add 

Helpful Commands
    Setup Admin User: pipenv run python manage.py createsuperuser
    Run tests: pipenv run test

Meaning of the files:
    ./Pipfile - used by pipenv, contains names and versions of python libraries
    ./Pipfile.lock
    ./db.sqlite3 - the local database used by this application
    ./formsite/__init__.py
    ./formsite/asgi.py
    ./formsite/settings.py - overall settings for the site
    ./formsite/urls.py - 'routes' - paths that are valid on the web site. includes urls from the ten app
    ./formsite/wsgi.py
    ./manage.py - used to manage django, e.g. manage.py makemigrations; manage.py migrate
    ./ten/__init__.py
    ./ten/admin.py - Standard django admin package for direct modification of database
    ./ten/apps.py - defines the applications valid in 'ten'
    ./ten/forms.py - define form element used on the ten/add page
    ./ten/migrations/0001_initial.py - changes to the database created by makemigrations after modifying models.py
    ./ten/migrations/__init__.py
    ./ten/models.py - Classes that define the database tables used
    ./ten/templates/ten/ten.html - html template that controls how the page appears in the browser
    ./ten/tests.py 
    ./ten/urls.py - 'routes' - paths that are valid in the ten app
    ./ten/views.py - routines that obtain, manipulate or save data to the model, called by html templates
