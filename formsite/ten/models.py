from django.db import models

class Mynum(models.Model):
    entered_at = models.DateTimeField(auto_now_add=True, null=True)
    number = models.IntegerField(null=True)
