from typing import ContextManager
from django.shortcuts import render

from .models import Mynum
from .forms import AddForm

def AddView(request):

    if request.method == 'POST':
        form_contents = AddForm(request.POST)
        if form_contents.is_valid():
            mynumber = form_contents.cleaned_data['my_number']
            m=Mynum(number=mynumber)
            m.save()
            last10 = Mynum.objects.all().order_by('-id')[:10]
            return render(request,'ten/ten.html',{ 'form': form_contents , 'last10': last10})
    else:
        form = AddForm()

    last10 = Mynum.objects.all().order_by('-id')[:10]
    return render(request, 'ten/ten.html', {'form': form , 'last10': last10})
